### Change log

##### v1.0.24
 - added missing dependecies for the cli

---


### 1. Setup

This project demonstrates features of `jacks-angular4-cli` and `jacks-angular4-test-utils`. The aim of creating this cli is to save developers' time to setup Jit, Aot, tsc, ngc, Lazy-loading, SystemJs, Webpack, Karma, library compilation, unit testing, coverage report and documentation. This cli is based on Gulp 4.

##### 1.1 Clone the demo project

navigate to where the project is to be saved, and then:

`$ git clone https://bitbucket.org/kdha200501/jacks-angular4-cli-demo.git`

##### 1.2 Install the cli and its dependencies

`$ cd jacks-angular4-cli-demo`

`$ npm i`

---

### 2. Build and run the demo project

The `jacks-angular4-cli` node module performs

- Jit build for `--dev`
- Aot for `--stage` and `--prod` environments

To make life simpler, the cli's environment flags are aliased as npm tasks shown below. Please also see the original cli flags in `package.json`.

##### 2.1 Dev build

`$ npm run build-dev`

>The `DemoPageModule` is compiled as CommonJs and lazy-loaded through Angular's compiler

##### 2.2 Dev build + serve + watch

`$ npm start`

##### 2.3 Prod build

`$ npm run build-prod`

>The `DemoPageModule` is compiled as es2015 and lazy-loaded through code-splitting

##### 2.4 Prod build + serve

`$ npm run start-prod`

>This build respects *config-**prod**.json* but takes *env-**dev**.json*

---

### 3. Unit-testing and coverage

Unit-testing is facilitated through Karma. The `jacks-angular4-test-utils` node module provides a means to spot-check the rendered views. Test id and test type tags are programmatically removed from HTML in both `stage` and `prod` builds.

`$ npm test`

>After a successful run, coverage report can be found in */dist/coverage-ts/index.html*

---

### 4. Library Compilation

The `jacks-angular4-test-utils` node module is a library of the bits and pieces of unit-testing, and this library is actually compiled from source through:

`$ npm run build-lib`

>After a successful run, add index.d.ts and index.js
>
>Remember to add .npmignore before publishing to npm

---

### 5. Documentation

JSDoc found in the `src` directory is woven into documentation:

`$ npm run doc`

>After a successful run, documentation can be found in */doc/index.html*