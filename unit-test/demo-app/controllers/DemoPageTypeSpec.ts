// dependencies
import {TestBed, async} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {ElementVerifierFactory} from 'jacks-angular4-test-utils';
// module classes
import {componentControllerList} from '../../../src/demo-app/Manifest';
// mocks
import {DemoPageMock} from '../../../src/demo-app/mocks/DemoPageMock';

describe('[DemoPage]', () => {

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                DemoPageMock,
                ...componentControllerList
            ],
            imports: [
                RouterTestingModule.withRoutes([{
                    path:      '',
                    component: DemoPageMock
                }])
            ]
        }).compileComponents();
    }));

    it('[Component: DemoPageBody]', () => {
        // create component and test fixture
        let fixture = TestBed.createComponent(DemoPageMock);
        // trigger change detection to update the view
        fixture.detectChanges();

        let testId = 'hello-world';
        let elementVerifier = ElementVerifierFactory.instantiate(fixture.debugElement, testId);
        expect(elementVerifier).toBeDefined();

        let testExpectation = 'hello world';
        expect( elementVerifier.match(testExpectation) ).toBe(true);

    });// endAll test cases

});// endAll unit tests
