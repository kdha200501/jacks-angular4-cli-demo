/*
global exports
*/

var MockResponse = (function () {
  "use strict";
  function MockResponse() {
  }

  MockResponse.entity1 = {
    "hello": "world"
  };

  return MockResponse;
}());

exports.MockResponse = MockResponse;
