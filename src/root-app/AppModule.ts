// dependencies
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {APP_BASE_HREF}   from '@angular/common';
import {RouterModule} from '@angular/router';
// this module
import {AppController} from './controllers/AppController';
// constants
import {CONFIG} from './config';
import {ENV} from './env';

@NgModule({
    // the controller to be exposed to SystemJs
    bootstrap:    [AppController],
    // controllers to be exposed to index.html
    declarations: [
        AppController
    ],
    providers:    [{
        provide:  APP_BASE_HREF,
        useValue: ENV['BASE_URL'] || '/'
    }],
    // nested modules and dependencies
    imports:      [
        BrowserModule,
        RouterModule.forRoot(CONFIG['routes'])
    ]
})
export class AppModule {
}// THE root app which is to be consumed by bootstrap, see main.ts
