import {Component} from '@angular/core';

@Component({
    selector: 'app-controller',
    template: `
        <p>
          <a routerLink="/">root app module</a>
        </p>
        <p>
          <a routerLink="/page">demo page module</a>
        </p>
        <router-outlet></router-outlet>
    `
})
export class AppController {
    constructor() {
    }
}
