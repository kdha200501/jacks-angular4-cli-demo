// dependencies
import {NgModule} from '@angular/core';
// nested classes
import {DemoPage} from './controllers/DemoPage';
import {componentControllerList} from './Manifest';
import {paths} from './paths';

@NgModule({
    declarations: [
        DemoPage,
        ...componentControllerList
    ],
    imports:      [paths]
})
export class DemoAppModule {
}// MVC for the "Page" content-type
