import {Routes, RouterModule} from '@angular/router';
import {DemoPage} from './controllers/DemoPage';

export const routes: Routes = [{
    path:      '',
    component: DemoPage

}];

export const paths = RouterModule.forChild(routes);
