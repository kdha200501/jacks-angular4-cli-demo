// dependencies
import {Component, forwardRef} from '@angular/core';
// mocks
import {DemoPage} from '../controllers/DemoPage';

@Component({
    template: `
        <demo-page-body></demo-page-body>
    `,
    providers: [{
        provide:     DemoPage,
        useExisting: forwardRef(() => DemoPageMock)
    }]
})
export class DemoPageMock {
    constructor() {}
}
