import {Component} from '@angular/core';

@Component({
    selector: 'demo-page-body',
    templateUrl: '../view/demo-app/controllers/demo-page-body.html',
    styleUrls:   ['../css/demo-app/controllers/demo-page-body.css']
})
export class DemoPageBody {
    constructor() {}
}
